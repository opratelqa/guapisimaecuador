package pages; 


import static org.testng.Assert.assertTrue;

import java.nio.charset.CodingErrorAction;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.swing.JOptionPane;

//@FindBy(how = How.XPATH,using = "/html/body/div[4]/div/div/div/div[2]/div/a[1]")
//@FindBy(how = How.XPATH,using = "//a[contains(.,'Tr�mites')]")
//private WebElement btnTramites;


import javax.swing.ScrollPaneLayout;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.sun.javafx.collections.MappingChange.Map;

import okhttp3.Call;
import test.TestBase;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import test.TestBase;
import test.ManipularExcel;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import org.testng.Assert;
import org.testng.annotations.Test;
import okhttp3.Call;
import test.TestBaseGuapisimaEcuador;

//import test.ManipularExcel;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;


 
public class GuapisimaEcuador extends TestBaseGuapisimaEcuador {
	
	final WebDriver driver;
	public GuapisimaEcuador(WebDriver driver){
		this.driver = driver;
		PageFactory.initElements(driver, this);
	} 	
	
	 
	/*
	 ******PASAR A BASEPAGE 
	 */
	public void WaitForElementClickable(By element, int tiempo) {
		WebDriverWait wait;
		wait = new WebDriverWait(driver, tiempo);
		wait.until(ExpectedConditions.elementToBeClickable(element));
	}

	public void WaitForElementClickable(WebElement element, int tiempo) {
		WebDriverWait wait;
		wait = new WebDriverWait(driver, tiempo);
		wait.until(ExpectedConditions.elementToBeClickable(element));
	}
	public void espera(int espera) {
		try {Thread.sleep(espera);} catch (InterruptedException e) {e.printStackTrace();}	
	}
	
	public void cargando(int loadingPage) {
		
		//espera a que salga el elemento cargando
				try {
		    		waitForElementToDisappear(By.id("loader"), 300, driver);
		    	} catch (Exception e) {
		    		System.out.println("No se encontro el Elemento: " + e.getMessage());
		    	}
		 
	}
	
	
	
	/*
	 ***************WEBELEMENTS*********** 
	 */

	@FindBy(how = How.CLASS_NAME,using = "form-control")
	private List<WebElement> campos;

	@FindBy(how = How.ID,using = "number")
	private WebElement IngresaLineaTelefonica;
	
	@FindBy(how = How.ID,using = "acepto")
	private WebElement checkMayorDeEdad;
	
	@FindBy(how = How.ID,using = "si")
	private WebElement btnContinue;
	
	

	
	
	//************CONSTRUCTOR***********
		
	@FindBy(how = How.ID,using = "RankBorrar")
	private WebElement aProbar;

	//*****************************

	
	public void logInGuapisimaEcuador(String apuntaA) {		
	System.out.println();
	System.out.println("***************************************************************************");
	System.out.println();
	System.out.println("Inicio de Test Guapisima Ecuador - landing");
		
		WebElement menu1 = driver.findElement(By.xpath("//a[contains(text(), 'GUIAS GUIDECENTRAL')]"));
			menu1.click();
			espera(500);
				String elemento1 = driver.findElement(By.cssSelector(".page-title")).getText();			
					Assert.assertEquals(elemento1,"GUIAS GUIDECENTRAL");
					System.out.println(elemento1);
			espera(500);
			
		WebElement menu2 = driver.findElement(By.xpath("//a[contains(text(), 'hogar')]"));
			menu2.click();
			espera(500);
				String elemento2 = driver.findElement(By.cssSelector(".page-title")).getText();
					Assert.assertEquals(elemento2,"HOGAR");
					System.out.println(elemento2);
			espera(500);		
					
		WebElement menu3 = driver.findElement(By.xpath("//a[contains(text(), 'sexo y pareja')]"));
			menu3.click();
			espera(500);
				String elemento3 = driver.findElement(By.cssSelector(".page-title")).getText();
				Assert.assertEquals(elemento3,"SEXO Y PAREJA");
				System.out.println(elemento3);
			espera(500);
			
		WebElement menu4 = driver.findElement(By.xpath("//a[contains(text(), 'mujer profesional')]"));
			menu4.click();
			espera(500);
				String elemento4 = driver.findElement(By.cssSelector(".page-title")).getText();
				Assert.assertEquals(elemento4,"MUJER PROFESIONAL");
				System.out.println(elemento4);
			espera(500);
			
		WebElement menu5 = driver.findElement(By.xpath("//a[contains(text(), 'soy mamá')]"));
			menu5.click();
			espera(500);
				String elemento5 = driver.findElement(By.cssSelector(".page-title")).getText();
				Assert.assertEquals(elemento5,"SOY MAMÁ");
				System.out.println(elemento5);
			espera(500);
			
		WebElement menu6 = driver.findElement(By.xpath("//a[contains(text(), 'Vida Sana')]"));
			menu6.click();
			espera(500);
				String elemento6 = driver.findElement(By.cssSelector(".page-title")).getText();
				Assert.assertEquals(elemento6,"VIDA SANA");
				System.out.println(elemento6);
			espera(500);
			
			
		WebElement menu7 = driver.findElement(By.xpath("//a[contains(text(), 'Moda y Belleza')]"));
			menu7.click();
			espera(500);
				String elemento7 = driver.findElement(By.cssSelector(".page-title")).getText();
				Assert.assertEquals(elemento7,"MODA Y BELLEZA");
				System.out.println(elemento7);
			espera(500);
			
			
		//Sobre nosotras
		driver.get(apuntaA + "guapisima.ec/sobre-nosotras/");
			espera(500);
				String elemento8 = driver.findElement(By.cssSelector(".entry-title")).getText();
				Assert.assertEquals(elemento8,"SOBRE NOSOTRAS");
				System.out.println(elemento8);
			espera(500);
			
		//Terminos y condiciones
		driver.get(apuntaA + "guapisima.ec/terminos-y-condiciones/");	
			espera(500);
				String elemento9 = driver.findElement(By.cssSelector(".entry-title")).getText();
				Assert.assertEquals(elemento9,"TÉRMINOS Y CONDICIONES");
				System.out.println(elemento9);
			espera(500);
				
		System.out.println("Resultado Esperado:Deberan visualizarce las secciones de Guapisima Ecuador"); 
		System.out.println();
		System.out.println("Fin de Test Patitas Guapisima Ecuador - Landing");
			
	}			

	
}  

